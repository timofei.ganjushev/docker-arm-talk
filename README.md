# Docker Arm Talk

DEMO project for ARM Docker talk

[Original Repository](https://gitlab.com/timofei.ganjushev/docker-arm-talk)

_build.sh_ scripts contains **build** and **push** commands

_manifest.sh_ scripts contains **manifest** check command

_run.sh_ scripts contains **pull** and **running** commands

_switch.sh_ scripts contains **[context use](https://docs.docker.com/engine/reference/commandline/context_create/)** commands

### Building and running
- **default** uses default docker context on machine to build and run containers
- **emulation** uses emulation supporting docker context (node1)
- **cluseter** uses multiple contexts builded via buildx create and buildx append (node1 && node2)
- **remote** uses docker context on another machine connected by ssh (node2)

